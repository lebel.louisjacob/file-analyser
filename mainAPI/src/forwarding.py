#!/usr/bin/env python3

from os import path

from utils import config

def save_and_analyse_file(file_id, flask_file):
    __save_file(file_id, flask_file)
    __analyze_file(file_id)

def __save_file(file_id, flask_file):
    file_path = config.analysis_path_of(file_id)
    flask_file.save(file_path)

def __analyze_file(file_id):
    response = config.analyse_file_type(file_id)

    file_type = response['file-type']
    if file_type == 'elf':
        __analyse_elf_file(file_id)

def __analyse_elf_file(file_id):
    response = config.analyse_elf(file_id)
