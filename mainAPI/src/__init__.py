#!/usr/bin/env python3

from utils import config
from flask import Flask, request, jsonify
from forwarding import save_and_analyse_file
from utils.sql import FilesDB
from templates import (
    index as index_template,
    add as add_template,
    add_no_file as add_no_file_template)

flask_app = Flask(__name__)

@flask_app.route('/')
def index():
    return index_template

@flask_app.route('/add', methods=['GET', 'POST'])
def index_add():
    if request.method == 'POST':
        return post_index_add()

    return add_template

def post_index_add():
    flask_file = request.files.get('file')
    if not flask_file:
        return add_no_file_template

    with FilesDB() as database:
        file_id = database.find_next_file_id()

    save_and_analyse_file(file_id, flask_file)
    return jsonify({
        'code': 100,
        'message': 'file uploaded successfully with id: ' + str(file_id)
    })

config.run_flask(flask_app, __name__)
