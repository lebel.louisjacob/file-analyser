#!/usr/bin/env python3

index = '''
<!doctype html>
<head>
    <title>API Entry Point</title>
</head>
<body>
    <h1>Welcome to the file analisys API!</h1>
    <h2>This project have been written by Louis-Jacob Lebel.</h2>
</body>'''

add = '''
<!doctype html>
<head>
    <title>Upload new File</title>
</head>
<body>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
        <input type=file name=file>
        <input type=submit value=Upload>
    </form>
</body>'''

add_no_file = '''
<!doctype html>
<head>
    <title>Upload new File</title>
</head>
<body>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
        <input type=file name=file>
        <input type=submit value=Upload>
    </form>
    <h2 color=red>You must select exactly one file to upload... Well, unless you're not planning to use the service.</h2>
</body>'''
