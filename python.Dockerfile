FROM python:3.6

COPY requirements.txt /tmp
RUN pip install -r /tmp/requirements.txt

CMD python /src/__init__.py
