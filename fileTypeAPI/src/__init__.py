#!/usr/bin/env python3

from flask import Flask, request, jsonify
from utils import config
from utils.sql import FilesDB
from selection import magic_bytes_of, find_file_type

flask_app = Flask(__name__)

@flask_app.route('/', methods=['GET'])
def index():
    file_id = request.args['file-id']
    file_magic_bytes = magic_bytes_of(file_id)
    file_type = find_file_type(file_magic_bytes)

    with FilesDB() as database:
        database.store_file(int(file_id), file_magic_bytes, file_type)

    return jsonify({'file-type': file_type})

config.run_flask(flask_app, __name__)
