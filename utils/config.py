#!/usr/bin/env python3

import argparse
from os import path, getenv

def run_flask(flask_app, name):
    if name == '__main__':
        api_port = getenv('API_PORT')
        flask_app.run(debug=True, host='0.0.0.0', port=api_port)

def analysis_path_of(file_id):
    analysis_path = getenv('GUEST_ANALYSIS_PATH')
    return path.join(analysis_path, str(file_id))

def database_crendentials():
    return {
        'user': getenv('DATABASE_USER'),
        'password': getenv('DATABASE_PASSWORD'),
        'host': getenv('DATABASE_HOST'),
        'port': getenv('DATABASE_PORT'),
        'database': getenv('DATABASE_SCHEMA')
    }

def analyse_elf(file_id):
    return __make_api_request('ELF_API_HOST', 'ELF_API_PORT', file_id)

def analyse_file_type(file_id):
    return __make_api_request('FILE_TYPE_API_HOST', 'FILE_TYPE_API_PORT', file_id)

def __make_api_request(host_variable, port_variable, file_id):
    import requests
    host = getenv(host_variable)
    port = getenv(port_variable)
    url = 'http://{host}:{port}'.format(host=host, port=port)
    response = requests.get(url, {'file-id': file_id})
    return response.json()
